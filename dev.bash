#!/usr/bin/env bash

file=slides.tex

./gen_bib.bash $file
latexmk -pdf $file

base=$(basename $file .tex)
xdg-open $base.pdf

ls * | entr -s "./gen_bib.bash $file && latexmk -pdf $file"
