\documentclass[]{beamer}

\usetheme{Singapore}

\usepackage{tabularx}

\makeatletter
\setbeamertemplate{footline}
{
    \hspace{0.01\paperwidth}
    \begin{beamercolorbox}[wd=0.2\paperwidth]{}
        \insertshortauthor\hspace{1ex}(\insertshortinstitute)
    \end{beamercolorbox}
    \begin{beamercolorbox}[wd=0.55\paperwidth]{}
        \centering\insertshorttitle
    \end{beamercolorbox}
    \begin{beamercolorbox}[wd=0.2\paperwidth]{}
        \hfill\insertframenumber/\inserttotalframenumber
    \end{beamercolorbox}
    \vspace{1pt}
}
\makeatother

\newcommand*{\tocfootline}{
    \makeatletter
    \setbeamertemplate{footline}
    {
        \hspace{0.01\paperwidth}
        \begin{beamercolorbox}[wd=0.2\paperwidth]{}
            \insertshortauthor\hspace{1ex}(\insertshortinstitute)
        \end{beamercolorbox}
        \begin{beamercolorbox}[wd=0.55\paperwidth]{}
            \centering\insertshorttitle
        \end{beamercolorbox}
        \vspace{1pt}
    }
    \makeatother
}

\makeatletter
\let\beamer@writeslidentry@miniframeson=\beamer@writeslidentry
\def\beamer@writeslidentry@miniframesoff{
    \expandafter\beamer@ifempty\expandafter{\beamer@framestartpage}{}
    {
        \clearpage\beamer@notesactions
    }
}
\newcommand*{\miniframeson}{\let\beamer@writeslidentry=\beamer@writeslidentry@miniframeson}
\newcommand*{\miniframesoff}{\let\beamer@writeslidentry=\beamer@writeslidentry@miniframesoff}
\beamer@compresstrue
\makeatother

\usepackage{caption}
\usepackage{multirow}
\usepackage{siunitx}

\usepackage[
    sorting=none,
    bibstyle=authoryear,
    citestyle=authoryearsquare,
    maxbibnames=10,
    maxcitenames=10,
    mincitenames=1,
    backend=bibtex,
]{biblatex}
\addbibresource{bibliography.bib}
\addbibresource{custom.bib}
\renewcommand*{\bibfont}{\tiny}

\usepackage{tikz}
\usetikzlibrary{
    arrows.meta,
    backgrounds,
    fit,
    positioning,
}
\tikzset{
    eq/.style = {
        fill=blue!20,
        text width=12em,
        minimum height=2em,
        text centered,
        rounded corners,
        line width=0,
    },
    bound/.style = {
        fill=green!20,
        text width=13em,
        rounded corners,
        line width=0,
    },
    arrow/.style = {
        -{>[length=2mm, width=3mm]},
    },
    highlight/.style = {
        fill=\light{#1},
        rounded corners,
    },
    text label/.style = {
        text width=10em,
        align=center,
    }
}
\usepackage{pgfplots}

\newcommand{\light}[1]{#1!20}
\newcommand{\medium}[1]{#1!40}

\newcommand{\pcite}[1]{\textcolor{gray}{\tiny\parencite{#1}}}

\newcommand{\osection}[3][]{
    \ifthenelse{\equal{#1}{}}{\section{#2}}{\section[#1]{#2}}

    \begingroup
    \miniframesoff
    \tocfootline
    \begin{frame}[noframenumbering]{\textcolor{\light{blue}}{Outline}}
        \begin{columns}
            \begin{column}{0.5\textwidth}
                \hfill
                \begin{minipage}[t][0.7\textheight]{0.9\textwidth}
                    \raggedright\tableofcontents[currentsection]
                \end{minipage}
            \end{column}
            \begin{column}{0.5\textwidth}
                \begin{figure}
                    \includegraphics[width=0.8\textwidth]{#3}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
    \endgroup
}

\graphicspath{{./img/}}

\usefonttheme[onlymath]{serif}

\def\talktitle{Optimising hadron collider simulations using matrix element neural networks}

\title{\talktitle\vspace{-1ex}}
\author{Ryan Moodie}
\institute[Turin]{Turin University \\\vspace{3ex} with Joseph Aylett-Bullock and Simon Badger \\\vspace{2ex} \href{https://inspirehep.net/literature/1869065}{arXiv:2106.09474}}
\date{\href{https://indico.cern.ch/event/1203113/}{CMS Physics Generator Meetings} \\\vspace{1em} 7 Nov 2022}
\titlegraphic{
    \begin{tikzpicture}[x=7em]
        \def\l{3em}
        \node at (0,0) {\includegraphics[width=\l,height=\l,keepaspectratio]{erc_logo}};
        \node at (1,0) {\includegraphics[width=\l,height=\l,keepaspectratio]{torino_logo}};
        \node at (2,0) {\includegraphics[width=\l,height=\l,keepaspectratio]{infn_logo}};
    \end{tikzpicture}
}

\begin{document}
\nocite{!src}

\begingroup
\setbeamertemplate{footline}{}
\begin{frame}[noframenumbering]
    \titlepage
\end{frame}
\endgroup

\begingroup
\tocfootline
\begin{frame}[noframenumbering]{Outline}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \hfill
            \begin{minipage}[t][0.7\textheight]{0.9\textwidth}
                \raggedright\tableofcontents
            \end{minipage}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.8\textwidth]{intersect}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}
\endgroup

\osection[\texorpdfstring{$\gamma\gamma$}{Diphoton} pheno]{Diphoton phenomenology}{intersect-hep}

\begin{frame}{Precision frontier}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Better understand properties of SM
                \item Indirect probe of new physics by small deviations
                \item LHC demanding increasing precision
                \item Theory predictions at 1\%
                \item Requires $\ge$ NNLO QCD
                    \begin{align*}
                        \alpha_s\approx&\,0.1\\
                        \mathrm{d}\sigma=&\,\mathrm{d}\sigma^{(0)}+\alpha_s\,\mathrm{d}\sigma^{(1)} \\
                        &+ \alpha_s^2\,\mathrm{d}\sigma^{(2)}+\ldots
                    \end{align*}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{jets_v1}
                \captionsetup{labelformat=empty}
                \caption{\pcite{!collaboration:2114784}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Diphoton production}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Flurry of activity
                \item Important background for measuring Higgs properties
                \item Rich $2\to3$ kinematics offer attractive probes
                \item Uncertainties
                \item Gluon fusion
                    \begin{itemize}
                        \item Excellent testing ground for new technologies
                    \end{itemize}
                \item Loop-induced
                    \begin{itemize}
                        \item Challenges conventional phase space optimisations
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{ppaaj_mAA_1}
                \captionsetup{labelformat=empty, skip=0pt}
                \caption{\pcite{Chawdhry:2021hkp}}
                \includegraphics[width=0.9\textwidth]{Mg1g2_2}
                \captionsetup{labelformat=empty, skip=0pt}
                \caption{\pcite{Badger:2021ohm}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[noframenumbering]{Perturbative anatomy}
    \begin{tikzpicture}
        \node (main) {
            \includegraphics[width=0.9\textwidth]{perturbative}
        };
        \node at (1,-2.25) [highlight={green}] (LO) {LO};
        \node [right = 1em of LO.east, highlight={yellow}] (NLO) {NLO};
        \node [right = 1em of NLO.east, highlight={orange}] (NNLO) {NNLO};
        \node [right = 1em of NNLO.east, highlight={red}] (N3LO) {N$^3$LO};
        \node [below = 1em of NNLO.south, highlight={blue}] (LILO) {LO};
        \node [below = 1em of N3LO.south, highlight={cyan}] (LINLO) {NLO};
        \node [left = 1em of LILO.west] (LI) {Loop induced:};
    \end{tikzpicture}
\end{frame}

\begin{frame}{Hadron collider simulations}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Complicated
                    \begin{itemize}
                        \item[\textcolor{\medium{green}}{\textbullet}] High-multiplicity amplitudes
                        \item[\textcolor{\medium{red}}{\textbullet}] PDFs
                        \item Variable centre-of-mass scales
                        \item Phase space sampling
                        \item Phase space cuts
                        \item Jet clustering
                    \end{itemize}
                \item Expensive
                    \begin{itemize}
                        \item[]
                            \begin{tabular}{l r}
                                \hline
                                Correction & CPU hours \\
                                \hline
                                Virtual & \num{50000} \\
                                Real & \num{500000} \\
                                \hline
                            \end{tabular}
                            \vspace{1ex}
                        \item[] \pcite{Badger:2021ohm}
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \vspace*{-2em}
                \hspace*{-2em}
                \includegraphics[width=1.2\textwidth]{collision}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\osection[ML]{Machine learning}{intersect-ml}

\begin{frame}{Neural networks}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \begin{center}
                    \begin{tikzpicture}[scale=0.35]
                        \begin{axis}[
                                xmin=-2,
                                xmax=2,
                                ymin=-2,
                                ymax=2,
                                xlabel=$x$,
                                ylabel=$\tanh(x)$,
                                axis lines=center,
                                x label style={at={(axis description cs:1.0,0.52)},anchor=south},
                                y label style={at={(axis description cs:0.42,1.0)},anchor=east},
                            ]
                            \addplot[
                                color=blue!35,
                                ultra thick,
                                samples=100,
                            ] plot[smooth] {tanh(x)};
                        \end{axis}
                    \end{tikzpicture}
                    \begin{tikzpicture}[scale=0.35]
                        \begin{axis}[
                                xmin=-2,
                                xmax=2,
                                ymin=-2,
                                ymax=2,
                                xlabel=$x$,
                                ylabel=$\mathrm{ReLU}(x)$,
                                axis lines=center,
                                x label style={at={(axis description cs:1.0,0.52)},anchor=south},
                                y label style={at={(axis description cs:0.42,1.0)},anchor=east},
                            ]
                            \addplot[
                                color=blue!35,
                                ultra thick,
                            ] coordinates {(-2,0)(0,0)(2,2)};
                        \end{axis}
                    \end{tikzpicture}
                \end{center}
            \end{figure}
            \vspace{1em}
            \begin{equation*}
                n^{(\ell)}_j = \underbrace{\strut f^{(\ell)}}_{\substack{\text{Activation}\\\text{function}}}\Big( \underbrace{\strut w^{(\ell)}_{ji}}_{\text{Weight}} \underbrace{\strut n^{(\ell-1)}_i}_{\text{Node}} + \underbrace{\strut b^{(\ell)}}_{\text{Bias}} \Big)
            \end{equation*}
            \vspace{0.5em}
            \begin{center}
                \textcolor{blue!70!black}{\textbullet} Classification \hspace{2em} \textcolor{blue!70!black}{\textbullet} Regression
            \end{center}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{nn}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Supervised learning}
    \begin{figure}
        \includegraphics[width=0.5\textwidth]{grad_desc}
    \end{figure}
    \vspace{-1em}
    \begin{description}[leftmargin=10em]
        \item[Neural network]
            \begin{itemize}
                \item $f:\mathbb{R}^{d_i}\to\mathbb{R}^{d_o}$
            \end{itemize}
        \item[Target dist.]
            \begin{itemize}
                \item $\left\{ (x_i,y_i) \mid {\small i\in\{1,\ldots,n\}} \right\}$
                \item Training data
            \end{itemize}
        \item[Loss function]
            \begin{itemize}
                \item Mean-squared error
                \item $L=\frac{1}{n}\sum_{i=1}^{n}\left(f(x_i)-y_i\right)^2$
            \end{itemize}
        \item[Optimise]
            \begin{itemize}
                \item Weights and biases
                \item Minimise $L$
                \item Gradient descent
                \item Backpropagation
            \end{itemize}
    \end{description}
\end{frame}

\begin{frame}{Applications in event generators}
    \begin{description}
        \item[Phase space sampling]
            Importance sampling multi-channel mappings, phase space event distributions
        \item[Parton distribution functions]
            Fitting from data, interpolation grids
        \item[Loop integrals]
            Integration contour in sector decomposition
        \item[Matrix elements]
            Emulation
        \item[Parton shower]
            Markov chain models, splitting kernel fitting
        \item[Fragmentation functions]
            Model-independent fitting
        \item[End-to-end generation]
        \item[\ldots]
            \pcite{Feickert:2021ajf}
    \end{description}
\end{frame}

\begin{frame}{Matrix element emulation}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Approximate matrix elements with neural networks
                    \begin{itemize}
                        \item Train models using amplitude libraries
                        \item[] \pcite{Bishara:2019iwh}
                        \item Tame IR behaviour
                        \item[] \pcite{Badger:2020uow}
                        \item Apply in realistic hadron collider simulations
                        \item[] \pcite{Aylett-Bullock:2021hmo} \pcite{Moodie:2022flt}
                        \item[] {\tiny\url{https://gitlab.com/JosephPB/n3jet_diphoton}}
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=0.8\textwidth]{ggyygg_1l}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\osection[ME NNs]{Matrix element neural networks}{intersect-menns}

\begin{frame}{Matrix elements}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Use amplitudes from \texttt{NJet}
                    \vspace{-1ex}
                \item[] \pcite{Badger:2012pg}
                    \vspace{0.5ex}
                \item[\textcolor{\medium{blue}}{\textbullet}] Numerical
                    \begin{itemize}
                        \item[\textcolor{\medium{blue}}{\textbullet}] Integrand reduction, generalised unitarity
                        \item[\textcolor{\medium{blue}}{\textbullet}] Automated but slow
                    \end{itemize}
                \item[\textcolor{orange}{\textbullet}] Analytical
                    \begin{itemize}
                        \item[\textcolor{orange}{\textbullet}] High multiplicity unavailable
                    \end{itemize}
                \item[\textcolor{green!65!black}{\textbullet}] Neural network model
                    \begin{itemize}
                        \item[\textcolor{green!65!black}{\textbullet}] Favourable time scaling
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{timing-single}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Data generation}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Extract from event generator
                \item Training
                    \begin{itemize}
                        \item 100k points
                        \item 4:1 training and validation sets
                    \end{itemize}
                \item Testing
                    \begin{itemize}
                        \item 3M points
                        \item Test with different random number seed
                    \end{itemize}
                \item Standardise input and output variable distributions
                    \begin{itemize}
                        \item Mean of 0
                        \item Standard deviation of 1
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{data_generation2}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Infrared behaviour}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Infrared divergences from soft $ s_i $ and collinear $ c_{ij} $ emissions
                \item Rapid local variations spoil global fit
                \item Naive model struggles
                \item Ensemble
                    \begin{itemize}
                        \item One network for each divergent structure
                        \item[] \pcite{Badger:2020uow}
                    \end{itemize}
                \item Alternative: boosting
                    \vspace{-1ex}
                \item[] \pcite{Badger:2022hwf}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{unit_error_plot_joint}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Phase space partitioning}
    \vspace{-1em}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Partition phase space
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{ir_partition_hor}
            \end{figure}
        \end{column}
    \end{columns}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item[\textcolor{\medium{cyan}}{\textbullet}] Tune threshold
                    \begin{itemize}
                        \item Each region contains points of similar scales
                        \item Sufficient points in \textcolor{red}{$ \mathcal{R}_{\text{div}} $}
                        \item Higher $y_\text{cut}$ more expensive
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{y_cut_tuning}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Subdivision of \texorpdfstring{\textcolor{red}{$ \mathcal{R}_{\text{div}} $}}{divergent region}}
    \begin{columns}
        \begin{column}{0.55\textwidth}
            \begin{itemize}
                \item Subdivide \textcolor{red}{$ \mathcal{R}_{\text{div}} $} using FKS
                    \vspace{-1ex}
                \item[] \pcite{Frederix:2009yq}
                    \vspace{1ex}
                \item $ \mathcal{P}_{\text{FKS}} = \left\lbrace (i,j) \mid s_i \lor s_j \lor c_{ij} \right\rbrace $
                    \vspace{2ex}
                \item $ \mathcal{S}_{ij} = 1 / \left( s_{ij} \sum_{k,\ell\in \mathcal{P}_{\text{FKS}}} \frac{1}{s_{k\ell}} \right) $
                    \begin{itemize}
                            \vspace{1ex}
                        \item Partition functions smoothly isolate singularities
                    \end{itemize}
                    \vspace{2ex}
                \item $ \lvert\mathcal{A}\rvert^2 = \sum_{i,j\in \mathcal{P}_{\text{FKS}}} \underbrace{\mathcal{S}_{ij} \lvert\mathcal{A}\rvert^2}_{\text{Network}} $
            \end{itemize}
        \end{column}
        \begin{column}{0.45\textwidth}
            \begin{figure}
                \includegraphics[width=0.6\textwidth]{blobs}
            \end{figure}
            \begin{figure}
                \includegraphics[width=\textwidth]{ir_ensemble}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Network architecture}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item General architecture
                    \begin{itemize}
                        \item Ensemble, partition, process, cuts
                        \item Hyperparameter optimisation on $gg\rightarrow\gamma\gamma g$
                    \end{itemize}
                \item Fully-connected neural network
                    \begin{itemize}
                        \item \texttt{Keras} with \texttt{TensorFlow} backend
                        \item[\textcolor{\medium{green}}{\textbullet}] $ n \times 4 $ input nodes
                        \item[\textcolor{\medium{blue}}{\textbullet}] 20-40-20 hidden nodes (hyperbolic-tangent)
                        \item[\textcolor{\medium{red}}{\textbullet}] Single output node (linear)
                    \end{itemize}
                \item Training by gradient descent
                    \begin{itemize}
                        \item Mean-squared-error loss function
                        \item Adam optimisation
                        \item Training epochs by Early Stopping
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{neural_network}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Uncertainties}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Infer on ensemble of 20 models
                    \begin{itemize}
                        \item Different random weight initialisation
                        \item Shuffled data sets
                    \end{itemize}
                \item Result
                    \begin{itemize}
                        \item Mean
                        \item Standard error (precision/optimality errors)
                    \end{itemize}
                    \vspace{1ex}
                \item Alternative: Bayesian networks
                    \vspace{-1ex}
                \item[] \pcite{Kasieczka:2020vlh}
                    \vspace{-1ex}
                \item[] \pcite{Badger:2022hwf}
            \end{itemize}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{stochastic_ensemble}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Interfacing with event generators}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Training in \texttt{Python}
                    \begin{itemize}
                        \item[\textcolor{\medium{blue}}{\textbullet}] Event generators in \texttt{C++}
                    \end{itemize}
                \item[\textcolor{\medium{orange}}{\textbullet}] \texttt{C++} inference code
                    \begin{itemize}
                        \item[\textcolor{\medium{orange}}{\textbullet}] Linear algebra by \texttt{Eigen3}
                        \item[] \pcite{!eigenweb}
                    \end{itemize}
                \item[\textcolor{\medium{blue}}{\textbullet}] Event generator: \texttt{Sherpa}
                \vspace{-1ex}
                \item[] \pcite{Bothmann:2019yzt}
                \item[\textcolor{\medium{green}}{\textbullet}] \texttt{C++} interface with \texttt{Sherpa}
                \item[\textcolor{\medium{blue}}{\textbullet}] \texttt{LHAPDF}, \texttt{NNPDF31\_nlo\_as\_0118}, \texttt{Rivet}
                \vspace{-1ex}
                \item[] \pcite{Buckley:2014ana,NNPDF:2017mvq,Bierlich:2019rhm}
            \end{itemize}

        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{interface2}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Pipeline overview}
    \begin{figure}
        \includegraphics[width=\textwidth,height=0.8\textheight,keepaspectratio]{pipeline2}
    \end{figure}
\end{frame}

\osection{Testing}{intersect-menns}

\begin{frame}{Methodology}
    \begin{columns}
        \begin{column}{0.7\textwidth}
            \begin{description}
                \item[Process]
                    \begin{itemize}
                        \item $ gg \to \gamma \gamma g$
                        \item $ gg \to \gamma \gamma gg$
                    \end{itemize}
                \item[Agreement]
                    \begin{itemize}
                        \item Matrix element
                        \item Total cross section
                        \item Differential cross section
                    \end{itemize}
                \item[Errors]
                    \begin{itemize}
                        \item \texttt{NJet}: Monte Carlo
                        \item Model: precision/optimality
                    \end{itemize}
                \item[$ gg \to \gamma \gamma g$]
                    \begin{itemize}
                        \item Cuts
                        \item Integrators
                    \end{itemize}
            \end{description}
        \end{column}
        \begin{column}{0.3\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{ggyyg_1l}
            \end{figure}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{ggyygg_1l}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Cuts and parameters}
    \begin{itemize}
        \item Typical diphoton LHC cuts
        \item[]
            \begin{align*}
                p_{T,j} &> \SI{20}{\GeV} & R_{\gamma j} &> 0.4 & |\eta_j| &< 5 \\
                p_{T,\gamma_1} &> \SI{40}{\GeV} & R_{\gamma \gamma} &> 0.4 & |\eta_{\gamma}| &< 2.37 \\
                p_{T,\gamma_2} &> \SI{30}{\GeV} & & & & \\
            \end{align*}
        \item Jets with anti-$k_T$ (\texttt{FastJet})
        \item Smooth photon isolation with $R=0.4$ and $\epsilon=0.05$
        \item $\sqrt{s_\text{hadronic}} = \SI{1}{\TeV}$
        \item $\mu_R = m_Z$
    \end{itemize}
\end{frame}

\osection{\texorpdfstring{$ gg \to \gamma \gamma g $}{gg->yyg}}{intersect-menns}

\begin{frame}{Per-point agreement}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Error distribution sufficiently
                    \vspace{-1ex}
                    \begin{itemize}
                        \item Narrow
                        \item Symmetric
                        \item Unit-centred
                    \end{itemize}
                    \vspace{1ex}
                \item Slight tail
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{unit_error_plot}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Integrators}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Integrator
                    \begin{itemize}
                        \item Unit grid
                        \item VEGAS
                    \end{itemize}
                \item Similar performance
                    \begin{itemize}
                        \item Slightly broader
                        \item More points in \textcolor{red}{$ \mathcal{R}_{\text{div}} $}
                    \end{itemize}
                \item Robust to adaptive sampling
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{unit_error_plot}
                \includegraphics[width=0.9\textwidth]{vegas_error_plot}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Shifted mean}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Gaussian error distribution
                    \begin{itemize}
                        \item Shifted mean
                    \end{itemize}
                \item \textcolor{red}{$ \mathcal{R}_{\text{div}} $} difficult to fit
                \item Gluon PDF suppresses these errors in cross section
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.9\textwidth]{unit_error_plot}
                \includegraphics[width=0.9\textwidth]{x2-error-plot}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Total cross section and cuts}
    \begin{figure}
        % see ./plot.py
        \hspace*{-4ex}
        \includegraphics[width=1.1\textwidth]{xs}
    \end{figure}
    \begin{itemize}
        \item Overlapping results
    \end{itemize}
\end{frame}

\begin{frame}{Distributions}
    \begin{columns}
        \begin{column}{0.33\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{five/deta_yy}
                \includegraphics[width=\textwidth]{five/dm_yy}
            \end{figure}
        \end{column}
        \begin{column}{0.33\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{five/dpt_j1}
                \includegraphics[width=\textwidth]{five/dpt_j2}
            \end{figure}
        \end{column}
        \begin{column}{0.33\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{five/dphi_jj}
                \includegraphics[width=\textwidth]{five/dr_jy}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\osection{\texorpdfstring{$ gg \to \gamma \gamma g g $}{gg->yygg}}{intersect-menns}

\begin{frame}{Per-point and total cross section}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Much more expensive process
                    \begin{itemize}
                        \item Numerical
                        \item $2\to4$ phase space
                    \end{itemize}
                \item Distribution
                    \begin{itemize}
                        \item Broader
                        \item More shifted
                    \end{itemize}
                \item Total cross section in agreement
                    \begin{itemize}
                        \item $ \sigma_\mathrm{NN} = (4.5 \pm 0.6) \times 10^{-6} \thinspace \mathrm{pb} $
                        \item $ \sigma_\mathrm{NJet} = (4.9 \pm 0.5) \times 10^{-6} \thinspace \mathrm{pb} $
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=0.85\textwidth]{error-plot-b=10-y=0.001-lin-overlaid}
                \includegraphics[width=0.85\textwidth]{error-plot-b=10-y=0.001-lin-overlaid-weighted}
                \captionsetup{labelformat=empty, skip=0pt}
                \caption{\pcite{!Moodie:2022dxs}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Distributions}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{dr_jy}
            \end{figure}
            \begin{itemize}
                \item Excellent Agreement
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{dpt_j1}
            \end{figure}
            \begin{itemize}
                \item Fluctuations are statistical
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\osection[Developments]{Recent developments}{intersect-menns}

\begin{frame}{Factorisation-aware matrix element emulation}
    \begin{columns}
        \begin{column}{0.6\textwidth}
            \begin{itemize}
                \item Hybrid: analytic infrared structure
                \item[] \pcite{Maitre:2021uaa} \pcite{!antenna}
            \end{itemize}
            \begin{figure}
                \includegraphics[width=\textwidth]{factorisation-arch}
            \end{figure}
        \end{column}
        \begin{column}{0.4\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{n3jet_comparison_inset}
                \captionsetup{labelformat=empty}
                \caption{\pcite{Maitre:2021uaa}}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}{Surrogate unweighting}
    \begin{figure}
        \includegraphics[width=0.8\textwidth]{surrogate}
        \captionsetup{labelformat=empty}
        \caption{\pcite{Danziger:2021eeg} \pcite{!surrogate}}
    \end{figure}
\end{frame}

\osection{Conclusion}{intersect}

\begin{frame}{Summary}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item Extend pioneering work on amplitude models to full hadron collider simulations
                    \begin{itemize}
                        \item Loop-induced $ gg \to \gamma\gamma + \text{jets} $
                        \item FKS partitioned phase space
                        \item Interface to \texttt{Sherpa}
                    \end{itemize}
                \item \textit{Provide general framework for optimising high-multiplicity observables}
                    \vspace{-1em}
                    \begin{itemize}
                        \item Excellent agreement in distributions
                        \item Simulation speed-up: $ N_{\text{infer}} / N_{\text{train}} $
                    \end{itemize}
            \end{itemize}
        \end{column}
        \begin{column}{0.25\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{deta_yy}
                \includegraphics[width=\textwidth]{dphi_jj}
            \end{figure}
        \end{column}
        \begin{column}{0.25\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{dm_yy}
                \includegraphics[width=\textwidth]{dpt_j2}
            \end{figure}
        \end{column}
    \end{columns}
\end{frame}

\appendix
\begingroup
\miniframesoff
\tocfootline

% \section*{Additional material}
% \begin{frame}[noframenumbering]{Title}
% \end{frame}

\begin{frame}[allowframebreaks,noframenumbering]{References}
    \printbibliography
\end{frame}
\endgroup

\end{document}
