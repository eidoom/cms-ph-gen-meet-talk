#!/usr/bin/env bash

file=$1
base=$(basename $file .tex)

./gen_bib.bash $file

pdflatex $file
bibtex $base
pdflatex $file
pdflatex $file
