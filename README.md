# [cms-ph-gen-meet-talk](https://gitlab.com/eidoom/cms-ph-gen-meet-talk)

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7760525.svg)](https://doi.org/10.5281/zenodo.7760525)

[Live here](https://eidoom.gitlab.io/cms-ph-gen-meet-talk/slides.pdf)

[Event](https://indico.cern.ch/event/1203113/)

[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)
